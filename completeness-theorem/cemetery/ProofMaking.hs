{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}

module ProofMaking (Proof, proveTrue, proveFalse, parseExpr) where

import           Control.Applicative
import           Control.Arrow ((>>>))
import qualified Data.ByteString.Char8 as T
import Data.List (nub, (\\))
import           Data.Char (isSpace)
import           Data.Function (on, (&))
import qualified Data.HashMap.Strict as M

import qualified AuxProofs as P
import           Expression
import           Lexer (tokenize)
import           Parser (parseInfo)
import           ProofManipulation (Annotated (annExpr), annotate)
import           Theorems (introduceHyp, substitute, substituteOne, helpers0)
import           Utils

type Proof = ([Expression], Expression, [Expression])

parseExpr :: T.ByteString -> Either String Expression
parseExpr s = tokenize s >>= parseInfo

breakHeader :: T.ByteString -> Either String ([Expression], Expression)
breakHeader header = do
    hyps <- hypsLine
          & T.dropWhile isSpace
          & T.split ','
          & map parseExpr
          & sequence
    stmt <- stmtLine & T.drop 2 & tokenize >>= parseInfo
    return (hyps, stmt)
  where
    (hypsLine, stmtLine) = T.breakSubstring "|-" header

getContext :: [T.ByteString] -> Either String Proof
getContext [] = Left "No header"
getContext (header:rest) = do
  (hyps, stmt) <- breakHeader header
  proof <- rest & map parseExpr & sequence
  return (hyps, stmt, proof)

parseProof :: T.ByteString -> Either String Proof
parseProof = T.split '\n' >>> init >>> getContext

type Mapping = M.HashMap String Bool

extractVariables :: Expression -> [String]
extractVariables expr = extractVariables' expr & nub
  where
    extractVariables' (a :-> b) = on (++) extractVariables a b
    extractVariables' (a :& b)  = on (++) extractVariables a b
    extractVariables' (a :| b)  = on (++) extractVariables a b
    extractVariables' (Not a)   = extractVariables a
    extractVariables' (Var s)   = [s]

truthTable :: Mapping -> Expression -> [Bool]
truthTable mapping expr = truthTable' mapping expr & map snd
  where
    truthTable' :: Mapping -> Expression -> [(Mapping, Bool)]
    truthTable' m (a :-> b) = binary (<=) m a b
    truthTable' m (a :& b) = binary (&&) m a b
    truthTable' m (a :| b) = binary (||) m a b
    truthTable' m (Not a) = (fmap not) <$> (truthTable' m a)
    truthTable' m (Var s)
      | Just value <- M.lookup s m = [(m, value)]
      | otherwise = do
          value <- [False, True]
          let m' = M.insert s value m
          return (m', value)

    binary op m a b = do
      (m', v1) <- truthTable' m a
      (m'', v2) <- truthTable' m' b
      return (m'', op v1 v2)

findMapping :: [String] -> Mapping -> Expression -> Bool -> Maybe Mapping
findMapping [] mapping stmt value
    | all (== value) (truthTable mapping stmt) = Just mapping
    | otherwise = Nothing
findMapping (x:xs) mapping stmt value
    = findMapping xs mapping stmt value
  <|> findMapping xs (M.insert x value mapping) stmt value

proveTrue :: Expression -> Maybe Proof
proveTrue stmt = do
    let vars = extractVariables stmt
    mapping <- findMapping vars M.empty stmt True
    let free = vars \\ M.keys mapping
    proof <- proveTrue' stmt mapping free
    let hyps = M.keys mapping & map Var
    return (hyps, stmt, proof)

prove :: Expression -> Bool -> Maybe Proof
prove stmt' defaultValue = do
    let stmt = if' defaultValue stmt' (Not stmt')
    let ctor = if' defaultValue Var (Not . Var)
    let vars = extractVariables stmt
    mapping <- findMapping vars M.empty stmt defaultValue
    let free = vars \\ M.keys mapping
    proof <- prove' stmt mapping free
    let hyps = M.keys mapping & map ctor
    return (hyps, stmt, proof)


prove' :: Expression -> Mapping -> [String] -> Maybe [Expression]
prove' stmt initM [] = build stmt initM
prove' stmt initM (x:xs) = do
    fNext <- prove' stmt (M.insert x False initM) xs
    tNext <- prove' stmt (M.insert x  initM) xs
    let hyps = M.keys initM & map Var
    annotatedFNext <- annotate hyps stmt fNext & eitherToMaybe
    annotatedTNext <- annotate hyps stmt fNext & eitherToMaybe
    let vx = Var x
    let unrolledFNext = introduceHyp (Not vx) annotatedFNext
    let unrolledTNext = introduceHyp vx annotatedTNext
    excludedMiddle vx stmt & eitherToMaybe

buildTrue :: Expression -> Mapping -> Maybe [Expression]
buildTrue (a :-> b) m
  | Just fa <- proveFalse' a m
  , Right whole <- buildProofIFA a b
    = return $ fa ++ whole
  | Just tb <- buildTrue b m
  , Right whole <- buildProofITB a b
    = return $ tb ++ whole
buildTrue (a :& b) m
  | Just ta <- buildTrue a m
  , Just tb <- buildTrue b m
  , Right whole <- buildProofCTATB a b
    = return $ ta ++ tb ++ whole
buildTrue (a :| b) m
  | Just ta <- buildTrue a m
  , Right whole <- buildProofDTA a b
    = return $ ta ++ whole
  | Just tb <- buildTrue b m
  , Right whole <- buildProofDTB a b
    = return $ tb ++ whole
buildTrue (Not a) m
  | Just fa <- proveFalse' a m
    = return fa
buildTrue (Var s) m
  | Just value <- M.lookup s m
    = if' value (Just [Var s]) Nothing
buildTrue _ _ = Nothing

proveFalse :: Expression -> Maybe Proof
proveFalse stmt = do
    mapping <- findMapping vars M.empty stmt False
    proof <- proveTrue' stmt mapping
    let hyps = M.keys mapping & map Var
    return (hyps, stmt, proof)
  where
    vars = extractVariables stmt

proveFalse' :: Expression -> Mapping -> Maybe [Expression]
proveFalse' (a :-> b) m
  | Just ta <- proveTrue' a m
  , Just fb <- proveFalse' b m
  , Right whole <- buildProofNITAFB a b
    = return $ ta ++ fb ++ whole
proveFalse' (a :& b) m
  | Just fa <- proveFalse' a m
  , Right whole <- buildProofNCFA a b
    = return $ fa ++ whole
  | Just fb <- proveFalse' a m
  , Right whole <- buildProofNCFB a b
    = return $ fb ++ whole
proveFalse' (a :| b) m
  | Just fa <- proveFalse' a m
  , Just fb <- proveFalse' b m
  , Right whole <- buildProofNDFAFB a b
    = return $ fa ++ fb ++ whole
proveFalse' (Not a) m
  | Just ta <- proveTrue' a m
  , Right whole <- buildProofNNTA a
    = return $ ta ++ whole
proveFalse' (Var s) m
  | Just value <- M.lookup s m
    = if' value Nothing (Just [Var s])
proveFalse' _ _ = Nothing

unrollProof :: String -> Expression -> Expression -> Either String [Annotated]
unrollProof strProof ea eb = do
  (hyps', stmt', proof') <- parseProof (T.pack strProof)
  let hyps = substitute [(Var "A", ea), (Var "B", eb)] hyps'
  let stmt = substituteOne [(Var "A", ea), (Var "B", eb)] stmt'
  let proof = substitute [(Var "A", ea), (Var "B", eb)] proof'
  annotated <- annotate hyps stmt proof
  return $ annotated

excludedMiddle :: Expression -> Expression -> Either String [Expression] -- a → b, -a → b ⊢ b
excludedMiddle ea eb = unrollProof P.excludedMiddle ea eb |> map (annExpr >>> hsExpr)

#define MAKE_NAME() buildProof

#define BUILD_PROOF2(name, ctor, excl, falseExclProof, trueExclProof) \
MAKE_NAME()name :: Expression -> Expression -> Either String [Expression]; \
MAKE_NAME()name ea eb = do {\
  df <- unrollProof (falseExclProof) ea eb |> introduceHyp (Not (excl)); \
  dt <- unrollProof (trueExclProof) ea eb |> introduceHyp (excl); \
  middle <- excludedMiddle eb ((ctor) ea eb); \
  return $ df ++ dt ++ middle; }

#define BUILD_PROOF1(name, strProof) \
MAKE_NAME()name :: Expression -> Expression -> Either String [Expression]; \
MAKE_NAME()name ea eb = unrollProof strProof ea eb |> map (annExpr >>> hsExpr)

BUILD_PROOF2(IFA, :->, eb, P.iFF, P.iFT)
BUILD_PROOF1(ITB, P.i_T)
BUILD_PROOF1(CTATB, P.cTT)
BUILD_PROOF1(DTA, P.dT_)
BUILD_PROOF1(DTB, P.d_T)
BUILD_PROOF1(NITAFB, P.niTF)
BUILD_PROOF1(NCFA, P.ncF_)
BUILD_PROOF1(NCFB, P.nc_F)
BUILD_PROOF1(NDFAFB, P.ndFF)

#undef MAKE_NAME
#undef BUILD_PROOF2
#undef BUILD_PROOF1

buildProofNNTA :: Expression -> Either String [Expression]
buildProofNNTA ea = Right $ substitute [(a, ea)] proof
  where
    a = Var "a"
    na = Not a
    nna = Not na
    proof =
      helpers0 na ++
      [ (na :-> a) :-> (na :-> na) :-> nna
      , a
      , a :-> na :-> a
      , na :-> a
      , (na :-> na) :-> nna
      , nna
      ]
