{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}

module ProofMaking (Proof, proveTrue, proveFalse) where

import           Control.Applicative
import           Control.Arrow ((>>>))
import           Data.Function (on, (&))
import qualified Data.HashMap.Strict as M
import           Data.List (nub, (\\))

import qualified AuxProofs as P
import           Expression
import           ParsingUtils (Proof)
import           ProofManipulation (Annotated (..), annotate)
import           Theorems (introduceHyp, substitute, substituteOne, helpers0)
import           Utils

type Mapping = M.HashMap String Bool

extractVariables :: Expression -> [String]
extractVariables expr = extractVariables' expr & nub
  where
    extractVariables' (a :-> b) = on (++) extractVariables a b
    extractVariables' (a :& b)  = on (++) extractVariables a b
    extractVariables' (a :| b)  = on (++) extractVariables a b
    extractVariables' (Not a)   = extractVariables a
    extractVariables' (Var s)   = [s]

truthTable :: Mapping -> Expression -> [Bool]
truthTable mapping expr = truthTable' mapping expr & map snd
  where
    truthTable' :: Mapping -> Expression -> [(Mapping, Bool)]
    truthTable' m (a :-> b) = binary (<=) m a b
    truthTable' m (a :& b) = binary (&&) m a b
    truthTable' m (a :| b) = binary (||) m a b
    truthTable' m (Not a) = (fmap not) <$> (truthTable' m a)
    truthTable' m (Var s)
      | Just value <- M.lookup s m = [(m, value)]
      | otherwise = do
          value <- [False, True]
          let m' = M.insert s value m
          return (m', value)

    binary op m a b = do
      (m', v1) <- truthTable' m a
      (m'', v2) <- truthTable' m' b
      return (m'', op v1 v2)

findMapping :: [String] -> Mapping -> Expression -> Bool -> Maybe Mapping
findMapping [] mapping stmt _
    | all (== True) (truthTable mapping stmt) = Just mapping
    | otherwise = Nothing
findMapping (x:xs) mapping stmt value
    = findMapping xs mapping stmt value
  <|> findMapping xs (M.insert x value mapping) stmt value

proveTrue :: Expression -> Maybe Proof
proveTrue stmt = prove stmt True

proveFalse :: Expression -> Maybe Proof
proveFalse stmt = prove stmt False

prove :: Expression -> Bool -> Maybe Proof
prove stmt' defaultValue = do
    let stmt = if' defaultValue stmt' (Not stmt')
    let ctor = if' defaultValue Var (Not . Var)
    let vars = extractVariables stmt
    mapping <- findMapping vars M.empty stmt defaultValue
    let free = vars \\ M.keys mapping
    proof <- prove' stmt mapping free
    let hyps = M.keys mapping & map ctor
    return (hyps, stmt, proof)

prove' :: Expression -> Mapping -> [String] -> Maybe [Expression]
prove' stmt mapping [] = buildT stmt mapping
prove' stmt mapping (x:xs) = do
    !fNext <- prove' stmt (M.insert x False mapping) xs
    !tNext <- prove' stmt (M.insert x True mapping) xs
    let hyps = M.toList mapping |> \(k, v) -> if' v (Var k) (Not (Var k))
    let vx = Var x
    !annotatedFNext <- annotate (Not vx : hyps) stmt fNext & eitherToMaybe
    !annotatedTNext <- annotate (vx : hyps) stmt tNext & eitherToMaybe
    let !unrolledFNext = introduceHyp (Not vx) annotatedFNext
    let !unrolledTNext = introduceHyp vx annotatedTNext
    excl <- excludedMiddle vx stmt & eitherToMaybe
    return $! unrolledFNext ++ unrolledTNext ++ excl

buildT :: Expression -> Mapping -> Maybe [Expression]
buildT (a :-> b) m
  | Just fa <- buildF a m
  , Right whole <- buildProofIFA a b
    = return $! fa ++ whole
  | Just tb <- buildT b m
  , Right whole <- buildProofITB a b
    = return $! tb ++ whole
buildT (a :& b) m
  | Just ta <- buildT a m
  , Just tb <- buildT b m
  , Right whole <- buildProofCTATB a b
    = return $! ta ++ tb ++ whole
buildT (a :| b) m
  | Just ta <- buildT a m
  , Right whole <- buildProofDTA a b
    = return $! ta ++ whole
  | Just tb <- buildT b m
  , Right whole <- buildProofDTB a b
    = return $! tb ++ whole
buildT (Not a) m
  | Just fa <- buildF a m
    = return $! fa
buildT (Var s) m
  | Just True <- M.lookup s m
    = Just [Var s]
buildT _ _ = Nothing

buildF :: Expression -> Mapping -> Maybe [Expression]
buildF (a :-> b) m
  | Just ta <- buildT a m
  , Just fb <- buildF b m
  , Right whole <- buildProofNITAFB a b
    = return $! ta ++ fb ++ whole
buildF (a :& b) m
  | Just fa <- buildF a m
  , Right whole <- buildProofNCFA a b
    = return $! fa ++ whole
  | Just fb <- buildF b m
  , Right whole <- buildProofNCFB a b
    = return $! fb ++ whole
buildF (a :| b) m
  | Just fa <- buildF a m
  , Just fb <- buildF b m
  , Right whole <- buildProofNDFAFB a b
    = return $! fa ++ fb ++ whole
buildF (Not a) m
  | Just ta <- buildT a m
  , Right whole <- buildProofNNTA a
    = return $! ta ++ whole
buildF (Var s) m
  | Just False <- M.lookup s m
    = Just [Not (Var s)]
buildF _ _ = Nothing


unrollProof :: Proof -> Expression -> Expression -> Either String [Annotated]
unrollProof genProof ea eb = do
    let (hyps', stmt', proof') = genProof
    annotated <- annotate hyps' stmt' proof'
    let substituted = annotated |> replaceInAnn
    return $ substituted
  where
    replaceInAnn :: Annotated -> Annotated
    replaceInAnn ann@Annotated {annExpr = h@Hashed {hsExpr = expr}} =
        let expr' = substituteOne [(Var "A", ea), (Var "B", eb)] expr
        in ann {annExpr = h {hsExpr = expr'}}

excludedMiddle :: Expression -> Expression -> Either String [Expression] -- a → b, -a → b ⊢ b
excludedMiddle ea eb = do
  proof <- P.excludedMiddle
  unrolled <- unrollProof proof ea eb
  return $ map (annExpr >>> hsExpr) unrolled

#define MAKE_NAME() buildProof

#define BUILD_PROOF2(name, ctor, excl, falseExclProof, trueExclProof) \
MAKE_NAME()name :: Expression -> Expression -> Either String [Expression]; \
MAKE_NAME()name ea eb = do {\
  fp <- falseExclProof; \
  tp <- trueExclProof; \
  df <- unrollProof fp ea eb |> introduceHyp (Not (excl)); \
  dt <- unrollProof tp ea eb |> introduceHyp (excl); \
  middle <- excludedMiddle eb ((ctor) ea eb); \
  return $ df ++ dt ++ middle; }

#define BUILD_PROOF1(name, genProof) \
MAKE_NAME()name :: Expression -> Expression -> Either String [Expression]; \
MAKE_NAME()name ea eb = do { \
  proof <- genProof; \
  unrolled <- unrollProof proof ea eb; \
  return $ map (annExpr >>> hsExpr) unrolled; }

BUILD_PROOF2(IFA, :->, eb, P.iFF, P.iFT)
BUILD_PROOF1(ITB, P.i_T)
BUILD_PROOF1(CTATB, P.cTT)
BUILD_PROOF1(DTA, P.dT_)
BUILD_PROOF1(DTB, P.d_T)
BUILD_PROOF1(NITAFB, P.niTF)
BUILD_PROOF1(NCFA, P.ncF_)
BUILD_PROOF1 (NCFB, P.nc_F)
BUILD_PROOF1(NDFAFB, P.ndFF)

#undef MAKE_NAME
#undef BUILD_PROOF2
#undef BUILD_PROOF1

buildProofNNTA :: Expression -> Either String [Expression]
buildProofNNTA ea = Right $ substitute [(a, ea)] proof
  where
    a = Var "a"
    na = Not a
    nna = Not na
    proof =
      helpers0 na ++
      [ (na :-> a) :-> (na :-> na) :-> nna
      , a
      , a :-> na :-> a
      , na :-> a
      , (na :-> na) :-> nna
      , nna
      ]
