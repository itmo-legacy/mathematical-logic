{-# LANGUAGE BangPatterns #-}
{-# OPTIONS_GHC -O2 #-}

module Main where

import           Control.Applicative
import           Control.Arrow ((>>>))
import           Control.Monad (unless)
import           Data.ByteString.Builder (char7, hPutBuilder, string7)
import qualified Data.ByteString.Char8 as T
import           Data.Function ((&))
import           Data.List (intercalate)
import           Data.Monoid
import           System.IO (stdout, stderr, hPutStrLn)

import           Expression (Hashed (hsExpr))
import           ParsingUtils (parseExpr)
import           ProofMaking (Proof, proveTrue, proveFalse)
import           ProofManipulation (Annotated (annExpr), refine)
import           ToString
import           Utils

printProof :: Proof -> IO ()
printProof (hyps, stmt, proof) = do
    hyps & map toString & intercalate ", " & putStr
    hPutStrLn stderr "finished here"
    unless (null hyps) $ putStr " "
    putStr "|- " >> putStrLn (toString stmt)
    proof & map toString & unlines & putStr
    -- proof & map toString & mapM_ putStrLn
    -- proof & map toString & map string7 & unlines_ & hPutBuilder stdout
  where unlines_ = map (<> char7 '\n') >>> mconcat

main :: IO ()
main = do
    maybeStmt <- T.getLine |> parseExpr
    case maybeStmt of
      Left !msg -> putStrLn $ "Parsing failure!\n" ++ msg
      Right !stmt ->
        case (proveTrue $! stmt) <|> (proveFalse $! stmt) of
          Nothing -> putStrLn ":("
          Just (hyps, stmt', !proof) ->
            case refine hyps stmt' proof of
              Left !msg -> putStrLn $ "Incorrect proof!\n" ++ msg
              Right !refined ->
                printProof (hyps, stmt', refined |> (annExpr >>> hsExpr))
