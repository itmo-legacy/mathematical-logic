{-# LANGUAGE BangPatterns      #-}
{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE OverloadedStrings #-}

module ProofManipulation (Annotated(..), Source(..), refine, annotate) where

import           Data.Function ((&))
import qualified Data.HashMap.Strict as M
import qualified Data.HashSet as S
import           Data.Hashable (Hashable)
import           Data.List (foldl')
import           Data.Maybe (catMaybes, listToMaybe)
import           Text.Printf (printf)

import           Expression (Expression (..), Hashed (..), hashed)
import           ToString
import           Utils

data Source = Hyp Int | Ax Int | MP Int Int deriving (Eq, Ord, Show)

data Annotated = Annotated
    { annI    :: Int
    , annSrc  :: Source
    , annExpr :: Hashed
    } deriving (Eq, Show)

instance ToString Source where
    {-# INLINE toString #-}
    toString (Hyp i)  = "Hypothesis " ++ show i
    toString (Ax i)   = "Ax. sch. " ++ show i
    toString (MP p q) = printf "M.P. %d, %d" p q

instance ToString Annotated where
    {-# INLINE toString #-}
    toString (Annotated i src expr) =
        printf "[%d. %s] %s" i (toString src) (toString expr)

type Hyps = M.HashMap Hashed Int
type STermMap = M.HashMap Hashed [(Annotated, Hashed)]
type ExprMap = M.HashMap Hashed Annotated

toMap :: (Hashable a, Eq a) => [a] -> M.HashMap a Int
toMap xs = zip [1 ..] xs & foldl' (\acc (i, x) -> M.insert x i acc) M.empty

refine :: [Expression] -> Expression -> [Expression] -> Either String [Annotated]
refine hyps stmt proof = annotate hyps stmt proof
                      |> minimise

annotate :: [Expression] -> Expression -> [Expression] -> Either String [Annotated]
annotate hyps stmt proof = annotate' (hyps & map hashed & toMap) (hashed stmt) (map hashed proof)

annotate' :: Hyps -> Hashed -> [Hashed] -> Either String [Annotated]
annotate' hyps stmt proof = go proof 1 M.empty M.empty []
  where
    go :: [Hashed] -> Int -> STermMap -> ExprMap -> [Annotated] -> Either String [Annotated]
    go [] _ _ _ acc = return $ reverse acc
    go [x] _ _ _ _
      | x /= stmt = Left "last statement is not what should be proved"
    go (x:xs) i table exprs acc
        | M.member x exprs = go xs i table exprs acc
        | (Just annx) <- annotated =
            let !i' = i + 1
                !acc' = (annx : acc)
                !table' = ntable annx
                !exprs' = M.insert x annx exprs
                !rest = go xs i' table' exprs' acc'
            in if x == stmt
                   then rest >> return (reverse (annx : acc))
                   else rest
        | otherwise = Left $ printf "statement #%d '%s' is unproved" i (toString x)
      where
        annotated =
            getSource x table exprs hyps >>= \src -> return $ Annotated i src x
        ntable annx
            | Hashed _ (e1 :-> e2) <- x =
                M.insertWith (++) (hashed e2) [(annx, hashed e1)] table
            | otherwise = table

getSource :: Hashed -> STermMap -> ExprMap -> Hyps -> Maybe Source
getSource he@(Hashed _ e) sterms exprs hyps
    | Just axi <- matchAxiom e = Just $ Ax axi
    | Just hypi <- he `M.lookup` hyps = Just $ Hyp hypi
    | Just (p, q) <- findMP he sterms exprs = Just $ MP p q
    | otherwise = Nothing

matchAxiom :: Expression -> Maybe Int
matchAxiom (a :-> _ :-> a')
    | a == a'
  = Just 1
matchAxiom ((a :-> b) :-> (a' :-> b' :-> c) :-> a'' :-> c')
    | a == a', a == a'', b == b', c == c'
  = Just 2
matchAxiom (a :-> b :-> a' :& b')
    | a == a' && b == b'
  = Just 3
matchAxiom (a :& _ :-> a')
    | a == a'
  = Just 4
matchAxiom (_ :& a :-> a')
    | a == a' = Just 5
matchAxiom (a :-> a' :| _)
    | a == a'
  = Just 6
matchAxiom (a :-> _ :| a')
    | a == a'
  = Just 7
matchAxiom ((a :-> c) :-> (b :-> c') :-> a' :| b' :-> c'')
    | a == a', b == b', c == c', c == c''
  = Just 8
matchAxiom ((a :-> b) :-> (a' :-> Not b') :-> Not a'')
    | a == a', a == a'', b == b'
  = Just 9
matchAxiom (Not (Not a) :-> a')
    | a == a'
  = Just 10
matchAxiom _
  = Nothing

findMP :: Hashed -> STermMap -> ExprMap -> Maybe (Int, Int)
findMP expr sterms exprs = candidates
                        |> map findPair
                        |> catMaybes
                       >>= listToMaybe
                        |> getIndices
  where
    candidates = M.lookup expr sterms
    findPair (annx, fterm) = M.lookup fterm exprs |> (,) annx
    getIndices (a, b) = (annI a, annI b)


minimise :: [Annotated] -> [Annotated]
minimise bloated = bloated
                 & reverse
                 & markNecessary (S.singleton (length bloated))
                &! filterOutUseless ([], M.empty) 1 bloated
                 & fixIndices
                 & reverse

type Table = M.HashMap Int Int
type IntSet = S.HashSet Int

markNecessary :: IntSet -> [Annotated] -> IntSet
markNecessary acc [] = acc
markNecessary acc (Annotated i (MP p q) _:xs)
    | i `S.member` acc = markNecessary acc' xs
  where
    !acc' = p `S.insert` (q `S.insert` acc)
markNecessary acc (_:xs) = markNecessary acc xs

filterOutUseless :: ([Annotated], Table) -> Int -> [Annotated] -> IntSet -> ([Annotated], Table)
filterOutUseless (acc, table) _ [] _ = (acc, table)
filterOutUseless (acc, table) ni (x:xs) necessary
    | i `S.member` necessary = filterOutUseless (acc', table') (ni + 1) xs necessary
    | otherwise = filterOutUseless (acc, table) ni xs necessary
  where
    i = annI x
    !acc' = x : acc
    !table' = M.insert i ni table

fixIndices :: ([Annotated], Table) -> [Annotated]
fixIndices (ann, table) = map fix ann
  where
    fix (Annotated i src e) = Annotated (fixi i) (fixsrc src) e
    fixi i = table M.! i
    fixsrc (MP p q) = MP (fixi p) (fixi q)
    fixsrc r        = r
