{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Expression (Expression(..), Hashed(..), hashed) where

import Data.Bits (xor)
import qualified Data.ByteString.Lazy.Char8 as T ()
import Data.Hashable (Hashable, hash, hashWithSalt)
import GHC.Generics (Generic)
import Text.Printf (printf)

import ToString

infixr 1 :->
infixl 2 :|
infixl 2 :&

data Expression
    = !Expression :-> !Expression
    | !Expression :| !Expression
    | !Expression :& !Expression
    | Not !Expression
    | Var !String
    deriving (Eq, Show, Ord, Generic, Hashable)

type Hash = Int

instance ToString Expression where
    {-# INLINE toString #-}
    toString = infixn

{-# INLINE infixn #-}
infixn :: Expression -> String
infixn (e1 :-> e2) = infixnWithSign "->" e1 e2
infixn (e1 :| e2)  = infixnWithSign "|" e1 e2
infixn (e1 :& e2)  = infixnWithSign "&" e1 e2
infixn (Not e)     = printf "!%s" (infixn e)
infixn (Var s)     = s

{-# INLINE infixnWithSign #-}
infixnWithSign :: String -> Expression -> Expression -> String
infixnWithSign s e1 e2 = printf "(%s %s %s)" (infixn e1) s (infixn e2)

data Hashed = Hashed
    { hsHash :: Hash
    , hsExpr :: Expression
    } deriving (Show)

instance Eq Hashed where
    (Hashed h1 _) == (Hashed h2 _) = h1 == h2

instance Hashable Hashed where
    {-# INLINE hashWithSalt #-}
    hashWithSalt salt (Hashed h _) = h `xor` (salt + 0x9e3779b9)

instance ToString Hashed where
    {-# INLINE toString #-}
    toString (Hashed _ e) = toString e

{-# INLINE hashed #-}
hashed :: Expression -> Hashed
hashed expr = Hashed (hash expr) expr
