{-# LANGUAGE OverloadedStrings #-}

module ParsingUtils (Proof, parseExpr, parseProof) where

import           Control.Arrow ((>>>))
import qualified Data.ByteString.Char8 as T
import           Data.Char (isSpace)
import           Data.Function ((&))

import           Expression (Expression)
import ToString
import           Lexer (tokenize)
import           Parser (parseInfo)

type Proof = ([Expression], Expression, [Expression])

parseExpr :: T.ByteString -> Either String Expression
parseExpr s = tokenize s >>= parseInfo

breakHeader :: T.ByteString -> Either String ([Expression], Expression)
breakHeader header = do
    hyps <- hypsLine
          & T.dropWhile isSpace
          & T.split ','
          & map parseExpr
          & sequence
    stmt <- stmtLine & T.drop 2 & tokenize >>= parseInfo
    return (hyps, stmt)
  where
    (hypsLine, stmtLine) = T.breakSubstring "|-" header

getContext :: [T.ByteString] -> Either String Proof
getContext [] = Left "No header"
getContext (header:rest) = do
  (hyps, stmt) <- breakHeader header
  proof <- rest & map parseExpr & sequence
  return (hyps, stmt, proof)

parseProof :: T.ByteString -> Either String Proof
parseProof = T.split '\n' >>> init >>> getContext
