{-# LANGUAGE OverloadedStrings #-}

module AuxProofs where

import qualified Data.ByteString.Char8 as T
import ParsingUtils (Proof, parseProof)

excludedMiddle :: Either String Proof
excludedMiddle = parseProof excludedMiddle'

excludedMiddle' :: T.ByteString
excludedMiddle' = "\
\A -> B, !A -> B |- B\n\
\(A)->((A)|!(A))\n\
\(!((A)|!(A))->((A)->!((A)|!(A))))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A)))\n\
\((!((A)|!(A))->((A)->!((A)|!(A))))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A))))->(((A)->((A)|!(A)))->((!((A)|!(A))->((A)->!((A)|!(A))))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A)))))\n\
\((A)->((A)|!(A)))->((!((A)|!(A))->((A)->!((A)|!(A))))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A))))\n\
\!((A)|!(A))->((A)->!((A)|!(A)))\n\
\(!((A)|!(A))->((A)->!((A)|!(A))))->(((A)->((A)|!(A)))->(!((A)|!(A))->((A)->!((A)|!(A)))))\n\
\((A)->((A)|!(A)))->(!((A)|!(A))->((A)->!((A)|!(A))))\n\
\(((A)->((A)|!(A)))->(!((A)|!(A))->((A)->!((A)|!(A)))))->((((A)->((A)|!(A)))->((!((A)|!(A))->((A)->!((A)|!(A))))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A)))))->(((A)->((A)|!(A)))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A)))))\n\
\(((A)->((A)|!(A)))->((!((A)|!(A))->((A)->!((A)|!(A))))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A)))))->(((A)->((A)|!(A)))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A))))\n\
\((A)->((A)|!(A)))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A)))\n\
\(!((A)|!(A))->((A)->((A)|!(A))))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A))))\n\
\((!((A)|!(A))->((A)->((A)|!(A))))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))))->(((A)->((A)|!(A)))->((!((A)|!(A))->((A)->((A)|!(A))))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A))))))\n\
\((A)->((A)|!(A)))->((!((A)|!(A))->((A)->((A)|!(A))))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))))\n\
\((A)->((A)|!(A)))->(!((A)|!(A))->((A)->((A)|!(A))))\n\
\(((A)->((A)|!(A)))->(!((A)|!(A))->((A)->((A)|!(A)))))->((((A)->((A)|!(A)))->((!((A)|!(A))->((A)->((A)|!(A))))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A))))))->(((A)->((A)|!(A)))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A))))))\n\
\(((A)->((A)|!(A)))->((!((A)|!(A))->((A)->((A)|!(A))))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A))))))->(((A)->((A)|!(A)))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))))\n\
\((A)->((A)|!(A)))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A))))\n\
\(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))\n\
\((((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))))->(((A)->((A)|!(A)))->((((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))))\n\
\((A)->((A)|!(A)))->((((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))))\n\
\((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))\n\
\(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))->(((A)->((A)|!(A)))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))\n\
\((A)->((A)|!(A)))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))\n\
\(((A)->((A)|!(A)))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->((((A)->((A)|!(A)))->((((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))))->(((A)->((A)|!(A)))->(!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))))\n\
\(((A)->((A)|!(A)))->((((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))))->(((A)->((A)|!(A)))->(!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))))\n\
\((A)->((A)|!(A)))->(!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))\n\
\(((A)->((A)|!(A)))->(!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A)))))->((((A)->((A)|!(A)))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))))->(((A)->((A)|!(A)))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))))\n\
\(((A)->((A)|!(A)))->((!((A)|!(A))->(((A)->((A)|!(A)))->(((A)->!((A)|!(A)))->!(A))))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))))->(((A)->((A)|!(A)))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A))))\n\
\((A)->((A)|!(A)))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))\n\
\(((A)->((A)|!(A)))->(!((A)|!(A))->(((A)->!((A)|!(A)))->!(A))))->((((A)->((A)|!(A)))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A))))->(((A)->((A)|!(A)))->(!((A)|!(A))->!(A))))\n\
\(((A)->((A)|!(A)))->((!((A)|!(A))->(((A)->!((A)|!(A)))->!(A)))->(!((A)|!(A))->!(A))))->(((A)->((A)|!(A)))->(!((A)|!(A))->!(A)))\n\
\((A)->((A)|!(A)))->(!((A)|!(A))->!(A))\n\
\!((A)|!(A))->!(A)\n\
\!(A)->((A)|!(A))\n\
\(!((A)|!(A))->(!(A)->!((A)|!(A))))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A)))\n\
\((!((A)|!(A))->(!(A)->!((A)|!(A))))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A))))->((!(A)->((A)|!(A)))->((!((A)|!(A))->(!(A)->!((A)|!(A))))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A)))))\n\
\(!(A)->((A)|!(A)))->((!((A)|!(A))->(!(A)->!((A)|!(A))))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A))))\n\
\!((A)|!(A))->(!(A)->!((A)|!(A)))\n\
\(!((A)|!(A))->(!(A)->!((A)|!(A))))->((!(A)->((A)|!(A)))->(!((A)|!(A))->(!(A)->!((A)|!(A)))))\n\
\(!(A)->((A)|!(A)))->(!((A)|!(A))->(!(A)->!((A)|!(A))))\n\
\((!(A)->((A)|!(A)))->(!((A)|!(A))->(!(A)->!((A)|!(A)))))->(((!(A)->((A)|!(A)))->((!((A)|!(A))->(!(A)->!((A)|!(A))))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A)))))->((!(A)->((A)|!(A)))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A)))))\n\
\((!(A)->((A)|!(A)))->((!((A)|!(A))->(!(A)->!((A)|!(A))))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A)))))->((!(A)->((A)|!(A)))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A))))\n\
\(!(A)->((A)|!(A)))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A)))\n\
\(!((A)|!(A))->(!(A)->((A)|!(A))))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A))))\n\
\((!((A)|!(A))->(!(A)->((A)|!(A))))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))))->((!(A)->((A)|!(A)))->((!((A)|!(A))->(!(A)->((A)|!(A))))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A))))))\n\
\(!(A)->((A)|!(A)))->((!((A)|!(A))->(!(A)->((A)|!(A))))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))))\n\
\(!(A)->((A)|!(A)))->(!((A)|!(A))->(!(A)->((A)|!(A))))\n\
\((!(A)->((A)|!(A)))->(!((A)|!(A))->(!(A)->((A)|!(A)))))->(((!(A)->((A)|!(A)))->((!((A)|!(A))->(!(A)->((A)|!(A))))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A))))))->((!(A)->((A)|!(A)))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A))))))\n\
\((!(A)->((A)|!(A)))->((!((A)|!(A))->(!(A)->((A)|!(A))))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A))))))->((!(A)->((A)|!(A)))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))))\n\
\(!(A)->((A)|!(A)))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A))))\n\
\((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))\n\
\(((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))))->((!(A)->((A)|!(A)))->(((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))))\n\
\(!(A)->((A)|!(A)))->(((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))))\n\
\(!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))\n\
\((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))->((!(A)->((A)|!(A)))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))\n\
\(!(A)->((A)|!(A)))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))\n\
\((!(A)->((A)|!(A)))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(((!(A)->((A)|!(A)))->(((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))))->((!(A)->((A)|!(A)))->(!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))))\n\
\((!(A)->((A)|!(A)))->(((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))))->((!(A)->((A)|!(A)))->(!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))))\n\
\(!(A)->((A)|!(A)))->(!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))\n\
\((!(A)->((A)|!(A)))->(!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A)))))->(((!(A)->((A)|!(A)))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))))->((!(A)->((A)|!(A)))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))))\n\
\((!(A)->((A)|!(A)))->((!((A)|!(A))->((!(A)->((A)|!(A)))->((!(A)->!((A)|!(A)))->!!(A))))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))))->((!(A)->((A)|!(A)))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A))))\n\
\(!(A)->((A)|!(A)))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))\n\
\((!(A)->((A)|!(A)))->(!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A))))->(((!(A)->((A)|!(A)))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A))))->((!(A)->((A)|!(A)))->(!((A)|!(A))->!!(A))))\n\
\((!(A)->((A)|!(A)))->((!((A)|!(A))->((!(A)->!((A)|!(A)))->!!(A)))->(!((A)|!(A))->!!(A))))->((!(A)->((A)|!(A)))->(!((A)|!(A))->!!(A)))\n\
\(!(A)->((A)|!(A)))->(!((A)|!(A))->!!(A))\n\
\(!((A)|!(A))->!!(A))\n\
\(!((A)|!(A))->!(A))->((!((A)|!(A))->!(!(A)))->!(!((A)|!(A))))\n\
\(!((A)|!(A))->!(!(A)))->!(!((A)|!(A)))\n\
\!(!((A)|!(A)))\n\
\!(!((A)|!(A)))->((A)|!(A))\n\
\(A)|!(A)\n\
\(A)->(B)\n\
\!(A)->(B)\n\
\((A)->(B))->((!(A)->(B))->(((A)|!(A))->(B)))\n\
\(!(A)->(B))->(((A)|!(A))->(B))\n\
\((A)|!(A))->(B)\n\
\(B)\n\
\"

iFF :: Either String Proof
iFF = parseProof iFF'

iFF' :: T.ByteString
iFF' = "\
\!A, !B |- A -> B\n\
\!(A)\n\
\(!(A)->((A)->!(A)))\n\
\((A)->!(A))\n\
\!(B)\n\
\(!(B)->((A)->!(B)))\n\
\((A)->!(B))\n\
\((A)->((A)->(A)))\n\
\((A)->(((A)->(A))->(A)))\n\
\(((A)->((A)->(A)))->(((A)->(((A)->(A))->(A)))->((A)->(A))))\n\
\(((A)->(((A)->(A))->(A)))->((A)->(A)))\n\
\((A)->(A))\n\
\((!(B)->(A))->((!(B)->!(A))->!!(B)))\n\
\(((!(B)->(A))->((!(B)->!(A))->!!(B)))->((A)->((!(B)->(A))->((!(B)->!(A))->!!(B)))))\n\
\((A)->((!(B)->(A))->((!(B)->!(A))->!!(B))))\n\
\((A)->(!(B)->(A)))\n\
\(((A)->(!(B)->(A)))->((A)->((A)->(!(B)->(A)))))\n\
\((A)->((A)->(!(B)->(A))))\n\
\(((A)->(A))->(((A)->((A)->(!(B)->(A))))->((A)->(!(B)->(A)))))\n\
\(((A)->((A)->(!(B)->(A))))->((A)->(!(B)->(A))))\n\
\((A)->(!(B)->(A)))\n\
\(!(A)->(!(B)->!(A)))\n\
\((!(A)->(!(B)->!(A)))->((A)->(!(A)->(!(B)->!(A)))))\n\
\((A)->(!(A)->(!(B)->!(A))))\n\
\(((A)->!(A))->(((A)->(!(A)->(!(B)->!(A))))->((A)->(!(B)->!(A)))))\n\
\(((A)->(!(A)->(!(B)->!(A))))->((A)->(!(B)->!(A))))\n\
\((A)->(!(B)->!(A)))\n\
\(((A)->(!(B)->(A)))->(((A)->((!(B)->(A))->((!(B)->!(A))->!!(B))))->((A)->((!(B)->!(A))->!!(B)))))\n\
\(((A)->((!(B)->(A))->((!(B)->!(A))->!!(B))))->((A)->((!(B)->!(A))->!!(B))))\n\
\((A)->((!(B)->!(A))->!!(B)))\n\
\(((A)->(!(B)->!(A)))->(((A)->((!(B)->!(A))->!!(B)))->((A)->!!(B))))\n\
\(((A)->((!(B)->!(A))->!!(B)))->((A)->!!(B)))\n\
\((A)->!!(B))\n\
\(!!(B)->(B))\n\
\((!!(B)->(B))->((A)->(!!(B)->(B))))\n\
\((A)->(!!(B)->(B)))\n\
\(((A)->!!(B))->(((A)->(!!(B)->(B)))->((A)->(B))))\n\
\(((A)->(!!(B)->(B)))->((A)->(B)))\n\
\((A)->(B))\n\
\"

i_T :: Either String Proof
i_T = parseProof i_T'

i_T' :: T.ByteString
i_T' = "\
\B |- A -> B\n\
\B\n\
\B -> A -> B\n\
\A -> B\n\
\"

iFT :: Either String Proof
iFT = parseProof iFT'

iFT' :: T.ByteString
iFT' = "\
\!A, B |- A -> B\n\
\(B)->((A)->(B))\n\
\(B)\n\
\(A)->(B)\n\
\"

cTT :: Either String Proof
cTT = parseProof cTT'

cTT' :: T.ByteString
cTT' = "\
\A, B |- A & B\n\
\A\n\
\B\n\
\A -> B -> A & B\n\
\B -> A & B\n\
\A & B\n\
\"

dT_ :: Either String Proof
dT_ = parseProof dT_'

dT_' :: T.ByteString
dT_' = "\
\A |- A | B\n\
\A\n\
\A -> A | B\n\
\A | B\n\
\"

d_T :: Either String Proof
d_T = parseProof d_T'

d_T' :: T.ByteString
d_T' = "\
\B |- A | B\n\
\B\n\
\B -> A | B\n\
\A | B\n\
\"

dFT :: Either String Proof
dFT = parseProof dFT'

dFT' :: T.ByteString
dFT' = "\
\!A, B |- A | B\n\
\B\n\
\B -> A | B\n\
\A | B\n\
\"

niTF :: Either String Proof
niTF = parseProof niTF'

niTF' :: T.ByteString
niTF' = "\
\A, !B |- !(A -> B)\n\
\(A)\n\
\!(B)\n\
\!(B)->(((A)->(B))->!(B))\n\
\((A)->(B))->!(B)\n\
\((A)->(((A)->(B))->(A)))\n\
\(((A)->(B))->(A))\n\
\(((A)->(B))->(((A)->(B))->((A)->(B))))\n\
\((((A)->(B))->(((A)->(B))->((A)->(B))))->((((A)->(B))->((((A)->(B))->((A)->(B)))->((A)->(B))))->(((A)->(B))->((A)->(B)))))\n\
\((((A)->(B))->((((A)->(B))->((A)->(B)))->((A)->(B))))->(((A)->(B))->((A)->(B))))\n\
\(((A)->(B))->((((A)->(B))->((A)->(B)))->((A)->(B))))\n\
\(((A)->(B))->((A)->(B)))\n\
\((((A)->(B))->(A))->((((A)->(B))->((A)->(B)))->(((A)->(B))->(B))))\n\
\((((A)->(B))->((A)->(B)))->(((A)->(B))->(B)))\n\
\(((A)->(B))->(B))\n\
\(((A)->(B))->(B))->((((A)->(B))->!(B))->!((A)->(B)))\n\
\(((A)->(B))->!(B))->!((A)->(B))\n\
\!((A)->(B))\n\
\"

ncF_ :: Either String Proof
ncF_ = parseProof ncF_'

ncF_' :: T.ByteString
ncF_' = "\
\!A |- !(A & B)\n\
\((A)&(B))->(A)\n\
\!(A)\n\
\!(A)->(((A)&(B))->!(A))\n\
\((A)&(B))->!(A)\n\
\(((A)&(B))->(A))->((((A)&(B))->!(A))->!((A)&(B)))\n\
\(((A)&(B))->!(A))->!((A)&(B))\n\
\!((A)&(B))\n\
\"

nc_F :: Either String Proof
nc_F = parseProof nc_F'

nc_F' :: T.ByteString
nc_F' = "\
\!B |- !(A & B)\n\
\((A)&(B))->(B)\n\
\!(B)\n\
\!(B)->(((A)&(B))->!(B))\n\
\((A)&(B))->!(B)\n\
\(((A)&(B))->(B))->((((A)&(B))->!(B))->!((A)&(B)))\n\
\(((A)&(B))->!(B))->!((A)&(B))\n\
\!((A)&(B))\n\
\"

ndFF :: Either String Proof
ndFF = parseProof ndFF'

ndFF' :: T.ByteString
ndFF' = "\
\!A, !B |- !(A | B)\n\
\!(A)\n\
\!(B)\n\
\(((A)|(B))->(A))->((((A)|(B))->!(A))->!((A)|(B)))\n\
\!(A)->(((A)|(B))->!(A))\n\
\((A)|(B))->!(A)\n\
\!(A)\n\
\(!(A)->(((A)|(B))->!(A)))\n\
\(((A)|(B))->!(A))\n\
\!(B)\n\
\(!(B)->(((A)|(B))->!(B)))\n\
\(((A)|(B))->!(B))\n\
\(((A)|(B))->(((A)|(B))->((A)|(B))))\n\
\(((A)|(B))->((((A)|(B))->((A)|(B)))->((A)|(B))))\n\
\((((A)|(B))->(((A)|(B))->((A)|(B))))->((((A)|(B))->((((A)|(B))->((A)|(B)))->((A)|(B))))->(((A)|(B))->((A)|(B)))))\n\
\((((A)|(B))->((((A)|(B))->((A)|(B)))->((A)|(B))))->(((A)|(B))->((A)|(B))))\n\
\(((A)|(B))->((A)|(B)))\n\
\((A)->((A)->(A)))\n\
\(((A)->((A)->(A)))->(((A)|(B))->((A)->((A)->(A)))))\n\
\(((A)|(B))->((A)->((A)->(A))))\n\
\(((A)->((A)->(A)))->(((A)->(((A)->(A))->(A)))->((A)->(A))))\n\
\((((A)->((A)->(A)))->(((A)->(((A)->(A))->(A)))->((A)->(A))))->(((A)|(B))->(((A)->((A)->(A)))->(((A)->(((A)->(A))->(A)))->((A)->(A))))))\n\
\(((A)|(B))->(((A)->((A)->(A)))->(((A)->(((A)->(A))->(A)))->((A)->(A)))))\n\
\((A)->(((A)->(A))->(A)))\n\
\(((A)->(((A)->(A))->(A)))->(((A)|(B))->((A)->(((A)->(A))->(A)))))\n\
\(((A)|(B))->((A)->(((A)->(A))->(A))))\n\
\((((A)|(B))->((A)->((A)->(A))))->((((A)|(B))->(((A)->((A)->(A)))->(((A)->(((A)->(A))->(A)))->((A)->(A)))))->(((A)|(B))->(((A)->(((A)->(A))->(A)))->((A)->(A))))))\n\
\((((A)|(B))->(((A)->((A)->(A)))->(((A)->(((A)->(A))->(A)))->((A)->(A)))))->(((A)|(B))->(((A)->(((A)->(A))->(A)))->((A)->(A)))))\n\
\(((A)|(B))->(((A)->(((A)->(A))->(A)))->((A)->(A))))\n\
\((((A)|(B))->((A)->(((A)->(A))->(A))))->((((A)|(B))->(((A)->(((A)->(A))->(A)))->((A)->(A))))->(((A)|(B))->((A)->(A)))))\n\
\((((A)|(B))->(((A)->(((A)->(A))->(A)))->((A)->(A))))->(((A)|(B))->((A)->(A))))\n\
\(((A)|(B))->((A)->(A)))\n\
\!(B)\n\
\(!(B)->(((A)|(B))->!(B)))\n\
\(((A)|(B))->!(B))\n\
\(!(B)->((B)->!(B)))\n\
\((!(B)->((B)->!(B)))->(((A)|(B))->(!(B)->((B)->!(B)))))\n\
\(((A)|(B))->(!(B)->((B)->!(B))))\n\
\((((A)|(B))->!(B))->((((A)|(B))->(!(B)->((B)->!(B))))->(((A)|(B))->((B)->!(B)))))\n\
\((((A)|(B))->(!(B)->((B)->!(B))))->(((A)|(B))->((B)->!(B))))\n\
\(((A)|(B))->((B)->!(B)))\n\
\!(A)\n\
\(!(A)->(((A)|(B))->!(A)))\n\
\(((A)|(B))->!(A))\n\
\(!(A)->((B)->!(A)))\n\
\((!(A)->((B)->!(A)))->(((A)|(B))->(!(A)->((B)->!(A)))))\n\
\(((A)|(B))->(!(A)->((B)->!(A))))\n\
\((((A)|(B))->!(A))->((((A)|(B))->(!(A)->((B)->!(A))))->(((A)|(B))->((B)->!(A)))))\n\
\((((A)|(B))->(!(A)->((B)->!(A))))->(((A)|(B))->((B)->!(A))))\n\
\(((A)|(B))->((B)->!(A)))\n\
\((B)->((B)->(B)))\n\
\(((B)->((B)->(B)))->(((A)|(B))->((B)->((B)->(B)))))\n\
\(((A)|(B))->((B)->((B)->(B))))\n\
\((B)->(((B)->(B))->(B)))\n\
\(((B)->(((B)->(B))->(B)))->(((A)|(B))->((B)->(((B)->(B))->(B)))))\n\
\(((A)|(B))->((B)->(((B)->(B))->(B))))\n\
\(((B)->((B)->(B)))->(((B)->(((B)->(B))->(B)))->((B)->(B))))\n\
\((((B)->((B)->(B)))->(((B)->(((B)->(B))->(B)))->((B)->(B))))->(((A)|(B))->(((B)->((B)->(B)))->(((B)->(((B)->(B))->(B)))->((B)->(B))))))\n\
\(((A)|(B))->(((B)->((B)->(B)))->(((B)->(((B)->(B))->(B)))->((B)->(B)))))\n\
\((((A)|(B))->((B)->((B)->(B))))->((((A)|(B))->(((B)->((B)->(B)))->(((B)->(((B)->(B))->(B)))->((B)->(B)))))->(((A)|(B))->(((B)->(((B)->(B))->(B)))->((B)->(B))))))\n\
\((((A)|(B))->(((B)->((B)->(B)))->(((B)->(((B)->(B))->(B)))->((B)->(B)))))->(((A)|(B))->(((B)->(((B)->(B))->(B)))->((B)->(B)))))\n\
\(((A)|(B))->(((B)->(((B)->(B))->(B)))->((B)->(B))))\n\
\((((A)|(B))->((B)->(((B)->(B))->(B))))->((((A)|(B))->(((B)->(((B)->(B))->(B)))->((B)->(B))))->(((A)|(B))->((B)->(B)))))\n\
\((((A)|(B))->(((B)->(((B)->(B))->(B)))->((B)->(B))))->(((A)|(B))->((B)->(B))))\n\
\(((A)|(B))->((B)->(B)))\n\
\((!(A)->(B))->((!(A)->!(B))->!!(A)))\n\
\(((!(A)->(B))->((!(A)->!(B))->!!(A)))->(((A)|(B))->((!(A)->(B))->((!(A)->!(B))->!!(A)))))\n\
\(((A)|(B))->((!(A)->(B))->((!(A)->!(B))->!!(A))))\n\
\(((!(A)->(B))->((!(A)->!(B))->!!(A)))->((B)->((!(A)->(B))->((!(A)->!(B))->!!(A)))))\n\
\((((!(A)->(B))->((!(A)->!(B))->!!(A)))->((B)->((!(A)->(B))->((!(A)->!(B))->!!(A)))))->(((A)|(B))->(((!(A)->(B))->((!(A)->!(B))->!!(A)))->((B)->((!(A)->(B))->((!(A)->!(B))->!!(A)))))))\n\
\(((A)|(B))->(((!(A)->(B))->((!(A)->!(B))->!!(A)))->((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))))\n\
\((((A)|(B))->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((((A)|(B))->(((!(A)->(B))->((!(A)->!(B))->!!(A)))->((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))))->(((A)|(B))->((B)->((!(A)->(B))->((!(A)->!(B))->!!(A)))))))\n\
\((((A)|(B))->(((!(A)->(B))->((!(A)->!(B))->!!(A)))->((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))))->(((A)|(B))->((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))))\n\
\(((A)|(B))->((B)->((!(A)->(B))->((!(A)->!(B))->!!(A)))))\n\
\((B)->(!(A)->(B)))\n\
\(((B)->(!(A)->(B)))->(((A)|(B))->((B)->(!(A)->(B)))))\n\
\(((A)|(B))->((B)->(!(A)->(B))))\n\
\(((B)->(!(A)->(B)))->((B)->((B)->(!(A)->(B)))))\n\
\((((B)->(!(A)->(B)))->((B)->((B)->(!(A)->(B)))))->(((A)|(B))->(((B)->(!(A)->(B)))->((B)->((B)->(!(A)->(B)))))))\n\
\(((A)|(B))->(((B)->(!(A)->(B)))->((B)->((B)->(!(A)->(B))))))\n\
\((((A)|(B))->((B)->(!(A)->(B))))->((((A)|(B))->(((B)->(!(A)->(B)))->((B)->((B)->(!(A)->(B))))))->(((A)|(B))->((B)->((B)->(!(A)->(B)))))))\n\
\((((A)|(B))->(((B)->(!(A)->(B)))->((B)->((B)->(!(A)->(B))))))->(((A)|(B))->((B)->((B)->(!(A)->(B))))))\n\
\(((A)|(B))->((B)->((B)->(!(A)->(B)))))\n\
\(((B)->(B))->(((B)->((B)->(!(A)->(B))))->((B)->(!(A)->(B)))))\n\
\((((B)->(B))->(((B)->((B)->(!(A)->(B))))->((B)->(!(A)->(B)))))->(((A)|(B))->(((B)->(B))->(((B)->((B)->(!(A)->(B))))->((B)->(!(A)->(B)))))))\n\
\(((A)|(B))->(((B)->(B))->(((B)->((B)->(!(A)->(B))))->((B)->(!(A)->(B))))))\n\
\((((A)|(B))->((B)->(B)))->((((A)|(B))->(((B)->(B))->(((B)->((B)->(!(A)->(B))))->((B)->(!(A)->(B))))))->(((A)|(B))->(((B)->((B)->(!(A)->(B))))->((B)->(!(A)->(B)))))))\n\
\((((A)|(B))->(((B)->(B))->(((B)->((B)->(!(A)->(B))))->((B)->(!(A)->(B))))))->(((A)|(B))->(((B)->((B)->(!(A)->(B))))->((B)->(!(A)->(B))))))\n\
\(((A)|(B))->(((B)->((B)->(!(A)->(B))))->((B)->(!(A)->(B)))))\n\
\((B)->(!(A)->(B)))\n\
\(((B)->(!(A)->(B)))->(((A)|(B))->((B)->(!(A)->(B)))))\n\
\(((A)|(B))->((B)->(!(A)->(B))))\n\
\(!(B)->(!(A)->!(B)))\n\
\((!(B)->(!(A)->!(B)))->(((A)|(B))->(!(B)->(!(A)->!(B)))))\n\
\(((A)|(B))->(!(B)->(!(A)->!(B))))\n\
\((!(B)->(!(A)->!(B)))->((B)->(!(B)->(!(A)->!(B)))))\n\
\(((!(B)->(!(A)->!(B)))->((B)->(!(B)->(!(A)->!(B)))))->(((A)|(B))->((!(B)->(!(A)->!(B)))->((B)->(!(B)->(!(A)->!(B)))))))\n\
\(((A)|(B))->((!(B)->(!(A)->!(B)))->((B)->(!(B)->(!(A)->!(B))))))\n\
\((((A)|(B))->(!(B)->(!(A)->!(B))))->((((A)|(B))->((!(B)->(!(A)->!(B)))->((B)->(!(B)->(!(A)->!(B))))))->(((A)|(B))->((B)->(!(B)->(!(A)->!(B)))))))\n\
\((((A)|(B))->((!(B)->(!(A)->!(B)))->((B)->(!(B)->(!(A)->!(B))))))->(((A)|(B))->((B)->(!(B)->(!(A)->!(B))))))\n\
\(((A)|(B))->((B)->(!(B)->(!(A)->!(B)))))\n\
\(((B)->!(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B)))))\n\
\((((B)->!(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B)))))->(((A)|(B))->(((B)->!(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B)))))))\n\
\(((A)|(B))->(((B)->!(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B))))))\n\
\((((A)|(B))->((B)->!(B)))->((((A)|(B))->(((B)->!(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B))))))->(((A)|(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B)))))))\n\
\((((A)|(B))->(((B)->!(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B))))))->(((A)|(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B))))))\n\
\(((A)|(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B)))))\n\
\((((A)|(B))->((B)->(!(B)->(!(A)->!(B)))))->((((A)|(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B)))))->(((A)|(B))->((B)->(!(A)->!(B))))))\n\
\((((A)|(B))->(((B)->(!(B)->(!(A)->!(B))))->((B)->(!(A)->!(B)))))->(((A)|(B))->((B)->(!(A)->!(B)))))\n\
\(((A)|(B))->((B)->(!(A)->!(B))))\n\
\(((B)->(!(A)->(B)))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A)))))\n\
\((((B)->(!(A)->(B)))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A)))))->(((A)|(B))->(((B)->(!(A)->(B)))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A)))))))\n\
\(((A)|(B))->(((B)->(!(A)->(B)))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A))))))\n\
\((((A)|(B))->((B)->(!(A)->(B))))->((((A)|(B))->(((B)->(!(A)->(B)))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A))))))->(((A)|(B))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A)))))))\n\
\((((A)|(B))->(((B)->(!(A)->(B)))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A))))))->(((A)|(B))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A))))))\n\
\(((A)|(B))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A)))))\n\
\((((A)|(B))->((B)->((!(A)->(B))->((!(A)->!(B))->!!(A)))))->((((A)|(B))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A)))))->(((A)|(B))->((B)->((!(A)->!(B))->!!(A))))))\n\
\((((A)|(B))->(((B)->((!(A)->(B))->((!(A)->!(B))->!!(A))))->((B)->((!(A)->!(B))->!!(A)))))->(((A)|(B))->((B)->((!(A)->!(B))->!!(A)))))\n\
\(((A)|(B))->((B)->((!(A)->!(B))->!!(A))))\n\
\(((B)->(!(A)->!(B)))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A))))\n\
\((((B)->(!(A)->!(B)))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A))))->(((A)|(B))->(((B)->(!(A)->!(B)))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A))))))\n\
\(((A)|(B))->(((B)->(!(A)->!(B)))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A)))))\n\
\((((A)|(B))->((B)->(!(A)->!(B))))->((((A)|(B))->(((B)->(!(A)->!(B)))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A)))))->(((A)|(B))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A))))))\n\
\((((A)|(B))->(((B)->(!(A)->!(B)))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A)))))->(((A)|(B))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A)))))\n\
\(((A)|(B))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A))))\n\
\((((A)|(B))->((B)->((!(A)->!(B))->!!(A))))->((((A)|(B))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A))))->(((A)|(B))->((B)->!!(A)))))\n\
\((((A)|(B))->(((B)->((!(A)->!(B))->!!(A)))->((B)->!!(A))))->(((A)|(B))->((B)->!!(A))))\n\
\(((A)|(B))->((B)->!!(A)))\n\
\(!!(A)->(A))\n\
\((!!(A)->(A))->(((A)|(B))->(!!(A)->(A))))\n\
\(((A)|(B))->(!!(A)->(A)))\n\
\((!!(A)->(A))->((B)->(!!(A)->(A))))\n\
\(((!!(A)->(A))->((B)->(!!(A)->(A))))->(((A)|(B))->((!!(A)->(A))->((B)->(!!(A)->(A))))))\n\
\(((A)|(B))->((!!(A)->(A))->((B)->(!!(A)->(A)))))\n\
\((((A)|(B))->(!!(A)->(A)))->((((A)|(B))->((!!(A)->(A))->((B)->(!!(A)->(A)))))->(((A)|(B))->((B)->(!!(A)->(A))))))\n\
\((((A)|(B))->((!!(A)->(A))->((B)->(!!(A)->(A)))))->(((A)|(B))->((B)->(!!(A)->(A)))))\n\
\(((A)|(B))->((B)->(!!(A)->(A))))\n\
\(((B)->!!(A))->(((B)->(!!(A)->(A)))->((B)->(A))))\n\
\((((B)->!!(A))->(((B)->(!!(A)->(A)))->((B)->(A))))->(((A)|(B))->(((B)->!!(A))->(((B)->(!!(A)->(A)))->((B)->(A))))))\n\
\(((A)|(B))->(((B)->!!(A))->(((B)->(!!(A)->(A)))->((B)->(A)))))\n\
\((((A)|(B))->((B)->!!(A)))->((((A)|(B))->(((B)->!!(A))->(((B)->(!!(A)->(A)))->((B)->(A)))))->(((A)|(B))->(((B)->(!!(A)->(A)))->((B)->(A))))))\n\
\((((A)|(B))->(((B)->!!(A))->(((B)->(!!(A)->(A)))->((B)->(A)))))->(((A)|(B))->(((B)->(!!(A)->(A)))->((B)->(A)))))\n\
\(((A)|(B))->(((B)->(!!(A)->(A)))->((B)->(A))))\n\
\((((A)|(B))->((B)->(!!(A)->(A))))->((((A)|(B))->(((B)->(!!(A)->(A)))->((B)->(A))))->(((A)|(B))->((B)->(A)))))\n\
\((((A)|(B))->(((B)->(!!(A)->(A)))->((B)->(A))))->(((A)|(B))->((B)->(A))))\n\
\(((A)|(B))->((B)->(A)))\n\
\(((A)->(A))->(((B)->(A))->(((A)|(B))->(A))))\n\
\((((A)->(A))->(((B)->(A))->(((A)|(B))->(A))))->(((A)|(B))->(((A)->(A))->(((B)->(A))->(((A)|(B))->(A))))))\n\
\(((A)|(B))->(((A)->(A))->(((B)->(A))->(((A)|(B))->(A)))))\n\
\((((A)|(B))->((A)->(A)))->((((A)|(B))->(((A)->(A))->(((B)->(A))->(((A)|(B))->(A)))))->(((A)|(B))->(((B)->(A))->(((A)|(B))->(A))))))\n\
\((((A)|(B))->(((A)->(A))->(((B)->(A))->(((A)|(B))->(A)))))->(((A)|(B))->(((B)->(A))->(((A)|(B))->(A)))))\n\
\(((A)|(B))->(((B)->(A))->(((A)|(B))->(A))))\n\
\((((A)|(B))->((B)->(A)))->((((A)|(B))->(((B)->(A))->(((A)|(B))->(A))))->(((A)|(B))->(((A)|(B))->(A)))))\n\
\((((A)|(B))->(((B)->(A))->(((A)|(B))->(A))))->(((A)|(B))->(((A)|(B))->(A))))\n\
\(((A)|(B))->(((A)|(B))->(A)))\n\
\((((A)|(B))->((A)|(B)))->((((A)|(B))->(((A)|(B))->(A)))->(((A)|(B))->(A))))\n\
\((((A)|(B))->(((A)|(B))->(A)))->(((A)|(B))->(A)))\n\
\(((A)|(B))->(A))\n\
\(((A)|(B))->!(A))->!((A)|(B))\n\
\!((A)|(B))\n\
\"
