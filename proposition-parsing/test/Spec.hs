{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}

import           Test.QuickCheck

import qualified Data.ByteString.Lazy.Char8 as T

import           Expression (ast, parseExpression)
import           Parser (runP)

toAst :: T.ByteString -> String
toAst s = case runP parseExpression s of
  Just (s, _) -> ast s
  Nothing     -> "failure"

prop_test1 = toAst "!A&!B->!(A|B)" == "(->,(&,(!A),(!B)),(!(|,A,B)))"
prop_test2 = toAst "P1'->!QQ->!R10&S|!T&U&V" == "(->,P1',(->,(!QQ),(|,(&,(!R10),S),(&,(&,(!T),U),V))))"

return []
main = $(quickCheckAll)
