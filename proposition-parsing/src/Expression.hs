{-# LANGUAGE OverloadedStrings #-}

module Expression (Expression, parseExpression, ast) where

import           Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as T ()
import           Text.Printf (printf)

import           Parser (Parser, chainl1, chainr1, char, eof, satisfy, spaces, string)

data Expression = Implication Expression Expression
                | Disjunction Expression Expression
                | Conjunction Expression Expression
                | Negation Expression
                | Var String
                deriving Show

parseExpression :: Parser Expression
parseExpression = spaces >> parseImplication <* eof

parseImplication :: Parser Expression
parseImplication = chainr1 parseDisjunction (Implication <$ string "->")

parseDisjunction :: Parser Expression
parseDisjunction = chainl1 parseConjunction (Disjunction <$ char '|')

parseConjunction :: Parser Expression
parseConjunction = chainl1 parseNegation (Conjunction <$ char '&')

parseNegation :: Parser Expression
parseNegation = negation <|> parseVar <|> parens
  where
    negation = Negation <$> (char '!' >> parseNegation)
    parens = char '(' >> parseImplication <* char ')'

parseVar :: Parser Expression
parseVar = Var <$> ((:) <$> parseFirst <*> parseRest)
  where
    parseFirst = satisfy (`elem` letters)
    parseNext = satisfy (`elem` allowed)
    parseRest = many parseNext
    letters = ['A' .. 'Z']
    allowed = letters ++ ['0' .. '9'] ++ ['\'']

astWithSign :: String -> Expression -> Expression -> String
astWithSign s e1 e2 = printf "(%s,%s,%s)" s (ast e1) (ast e2)

ast :: Expression -> String
ast (Implication e1 e2) = astWithSign "->" e1 e2
ast (Disjunction e1 e2) = astWithSign "|" e1 e2
ast (Conjunction e1 e2) = astWithSign "&" e1 e2
ast (Negation e)        = printf "(!%s)" (ast e)
ast (Var s)             = s
