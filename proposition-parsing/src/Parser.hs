{-# LANGUAGE InstanceSigs      #-}
{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PatternSynonyms   #-}
{-# LANGUAGE ViewPatterns      #-}

module Parser (Parser(..), ok, eof, spaces, satisfy, char, string, chainl1, chainr1) where

import           Control.Applicative
import           Control.Arrow (first, (>>>))
import           Control.Monad ((>=>))
import qualified Data.ByteString.Lazy.Char8 as T
import           Data.Char (isSpace)
import           Data.Foldable (foldl')
import           Data.Function ((&))

newtype Parser a = Parser { runP :: T.ByteString -> Maybe (a, T.ByteString) }

instance Functor Parser where
  fmap :: (a -> b) -> Parser a -> Parser b
  fmap f (Parser p) = Parser (p >>> fmap (first f))

instance Applicative Parser where
  pure :: a -> Parser a
  pure a = Parser $ \s -> Just (a, s)

  (<*>) :: Parser (a -> b) -> Parser a -> Parser b
  Parser pf <*> Parser pa = Parser $ \s -> do
    (f, t) <- pf s
    (a, r) <- pa t
    return (f a, r)

instance Monad Parser where
  (>>=) :: Parser a -> (a -> Parser b) -> Parser b
  Parser pa >>= f = Parser $ pa >=> (\(a, t) -> runP (f a) t)

instance Alternative Parser where
  empty :: Parser a
  empty = Parser $ const Nothing

  (<|>) :: Parser a -> Parser a -> Parser a
  Parser pa1 <|> Parser pa2 = Parser $ \s -> pa1 s <|> pa2 s

infixr 5 :<
pattern (:<) :: Char -> T.ByteString -> T.ByteString
pattern b :< bs <- (T.uncons -> Just (b, bs))

ok :: Monoid a => Parser a
ok = Parser $ \s -> Just (mempty, s)

eof :: Parser ()
eof = Parser $ \s -> case s of
  "" -> Just ((), "")
  _  -> Nothing

satisfy' :: (Char -> Bool) -> Parser Char
satisfy' p = Parser $ \s -> case s of
  ""      -> Nothing
  (c:<cs) -> if p c then Just (c, cs) else Nothing

spaces :: Parser T.ByteString
spaces = T.pack <$> many (satisfy' isSpace)

satisfy :: (Char -> Bool) -> Parser Char
satisfy p = satisfy' p <* spaces

char' :: Char -> Parser Char
char' c = satisfy' (== c)

char :: Char -> Parser Char
char c = satisfy (== c)

string' :: T.ByteString -> Parser T.ByteString
string' ""      = Parser $ \s -> Just ("", s)
string' (c:<cs) = T.cons <$> char' c <*> string' cs

string :: T.ByteString -> Parser T.ByteString
string s = string' s <* spaces

chainr1 :: Parser b -> Parser (b -> b -> b) -> Parser b
chainr1 term op = do
    init' <- many (term <**> op)
    z <- term
    return $ foldr ($) z init'

chainl1 :: Parser b -> Parser (b -> b -> b) -> Parser b
chainl1 term op = do
    a <- term
    rest <- many (flip <$> op <*> term)
    return $ foldl' (&) a rest
