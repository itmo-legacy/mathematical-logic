module Main where

import qualified Data.ByteString.Lazy.Char8 as T

import           Expression (parseExpression, ast)
import           Parser (runP)

main :: IO ()
main = do
    s <- T.getContents
    case runP parseExpression s of
        Just (e, _) -> putStrLn $ ast e
        Nothing -> putStrLn "damn"
