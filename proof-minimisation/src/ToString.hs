module ToString where

class ToString a where
    toString :: a -> String
