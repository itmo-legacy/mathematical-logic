{-# OPTIONS_GHC -O2 #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Control.Monad (unless)
import           Data.Array
import qualified Data.ByteString.Char8 as T
import           Data.Char (isSpace)
import           Data.Function ((&))
import qualified Data.HashMap.Strict as M
import           Data.Hashable (Hashable)
import           Data.List (foldl', intercalate)
import           Data.Tuple (swap)

import           Expression (Hashed, hashed)
import           Lexer (tokenize)
import           Parser (parseInfo)
import           Proof (refine)
import           ToString
import           Utils

toMap :: (Hashable a, Eq a) => [a] -> M.HashMap a Int
toMap xs = zip [1 ..] xs & foldl' (\acc (i, x) -> M.insert x i acc) M.empty

toList :: M.HashMap a Int -> [a]
toList m = M.toList m |> swap & array (1, M.size m) & elems

parseHashed :: T.ByteString -> Either String Hashed
parseHashed s = tokenize s >>= parseInfo |> hashed

breakHeader :: T.ByteString -> Either String (M.HashMap Hashed Int, Hashed)
breakHeader header = do
    hyps <- hypsLine
          & T.dropWhile isSpace
          & T.split ','
          & map parseHashed
          & sequence
    stmt <- stmtLine & T.drop 2 & tokenize >>= parseInfo
    return (toMap hyps, hashed stmt)
  where
    (hypsLine, stmtLine) = T.breakSubstring "|-" header

getContext :: [T.ByteString] -> Either String (M.HashMap Hashed Int, Hashed, [Hashed])
getContext [] = Left "No header"
getContext (header:rest) = do
  (hyps, stmt) <- breakHeader header
  proof <- rest & init & map parseHashed & sequence
  return (hyps, stmt, proof)

main :: IO ()
main = do
    contents <- T.getContents |> T.split '\n'
    case getContext contents of
        Left msg -> putStrLn $ "Parsing failure!\n" ++ msg
        Right (hyps, stmt, proof) ->
            case refine hyps stmt proof of
                Left err -> putStrLn ("Error: " ++ err)
                Right fineProof -> do
                    hyps & toList & map toString & intercalate ", " & putStr
                    unless (null hyps) $ putStr " "
                    putStr "|- " >> putStrLn (toString stmt)
                    fineProof & map toString & mapM_ putStrLn
