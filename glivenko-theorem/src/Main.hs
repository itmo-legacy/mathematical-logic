{-# OPTIONS_GHC -O2 #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Control.Monad (unless)
import qualified Data.ByteString.Char8 as T
import           Data.Char (isSpace)
import           Data.Function ((&))
import           Data.List (intercalate)

import           Expression (Expression (Not))
import           Lexer (tokenize)
import           Parser (parseInfo)
import           ProofManipulation (annotate)
import           Theorems (performGlivenkoTranslation)
import           ToString
import           Utils

parseExpr :: T.ByteString -> Either String Expression
parseExpr s = tokenize s >>= parseInfo

breakHeader :: T.ByteString -> Either String ([Expression], Expression)
breakHeader header = do
    hyps <- hypsLine
          & T.dropWhile isSpace
          & T.split ','
          & map parseExpr
          & sequence
    stmt <- stmtLine & T.drop 2 & tokenize >>= parseInfo
    return (hyps, stmt)
  where
    (hypsLine, stmtLine) = T.breakSubstring "|-" header

getContext :: [T.ByteString] -> Either String ([Expression], Expression, [Expression])
getContext [] = Left "No header"
getContext (header:rest) = do
  (hyps, stmt) <- breakHeader header
  proof <- rest & map parseExpr & sequence
  return (hyps, stmt, proof)

main :: IO ()
main = do
    contents <- T.getContents |> T.split '\n' |> init
    case getContext contents of
        Left msg -> putStrLn $ "Parsing failure!\n" ++ msg
        Right (hyps, stmt, proof) ->
            case annotate hyps stmt proof of
                Left err -> putStrLn ("Error: " ++ err)
                Right annotated -> do
                    let translation = performGlivenkoTranslation annotated
                    hyps & map toString & intercalate ", " & putStr
                    unless (null hyps) $ putStr " "
                    putStr "|- " >> putStrLn (toString $ Not $ Not stmt)
                    translation & map toString & mapM_ putStrLn
