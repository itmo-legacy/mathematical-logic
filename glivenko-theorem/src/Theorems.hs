{-# LANGUAGE PatternSynonyms #-}

module Theorems where

import qualified Data.HashMap.Strict as M
import           Data.Array.IArray
import           Data.Either (either)
import           Control.Arrow ((>>>))
import           Data.Function (on, (&))
import           Data.List (foldl')

import           Expression (Expression (..), Hashed (..))
import           ProofManipulation (Annotated (..), Source (..), annotate)
import           ToString
import           Utils

type IntArr a = Array Int a

extractWithSource :: [Annotated] -> IntArr (Expression, Source)
extractWithSource list = list
                       & listArray (1, length list)
                      |> (\ann -> (hsExpr $ annExpr ann, annSrc ann))

performGlivenkoTranslation :: [Annotated] -> [Expression]
performGlivenkoTranslation proof' = concatMap translateOne proof
  where
    proof = extractWithSource proof'

    translateOne :: (Expression, Source) -> [Expression]
    translateOne (expr, Hyp _) = translateTautology expr
    translateOne (expr, Ax _)
        | Just inner <- extract10th expr = translate10thScheme inner
        | otherwise = translateTautology expr
    translateOne (expr, MP _ q) = translateMP (proof ! q & fst) expr

    extract10th (NN a :-> a')
        | a == a' = Just a
    extract10th _ = Nothing

pattern N :: Expression -> Expression
pattern N e = Not e

pattern NN :: Expression -> Expression
pattern NN e = Not (Not e)

translateTautology :: Expression -> [Expression]
translateTautology ea = substitute [(a, ea)] proof  -- a ⊢ --a
  where
    a = Var "a"
    proof = concat
      [ [ (N a :-> a) :-> (N a :-> N a) :-> NN a -- 1. sch 9
        , a :-> N a :-> a                        -- 2. sch 1
        , a                                      -- 3. hyp
        , N a :-> a ]                            -- 4. MP 2,3
        , helpers0 (N a),                        -- 5. -a → -a
        [ (N a :-> N a) :-> NN a                 -- 6. MP 1,4
        , NN a ]                                 -- 7. MP 6,5
      ]

translate10thScheme :: Expression -> [Expression]
translate10thScheme ea = substitute [(a, ea)] proof -- ⊢ --(--a → a)
  where
    a = Var "a"
    sch10 = NN a :-> a
    proof = concat
      [ [ a :-> sch10                                              -- 1. sch 1
        , N a :-> sch10 ]                                          -- 2. sch 10 (IPC)
        , helpers1 a sch10                                         -- 3. -(--a → a) → -a
        , helpers1 (N a) sch10,                                    -- 4. -(--a → a) → --a
        [ (N sch10 :-> N a) :-> (N sch10 :-> NN a) :-> NN sch10    -- 5. sch 9
        , (N sch10 :-> NN a) :-> NN sch10                          -- 6. MP 3,5
        , NN sch10 ]                                               -- 7. MP 4,6
      ]


translateMP :: Expression -> Expression -> [Expression]
translateMP ea eb = substitute [(a, ea), (b, eb)] proof -- --(a → b), --a ⊢ --b
  where
    a = Var "a"
    b = Var "b"
    n4 = N b :-> NN a :& N b
    n5 = NN a :& N b :-> N (a :-> b)
    n6 = N b :-> n5
    n9 = N b :-> N (a :-> b)
    n10 = N b :-> NN (a :-> b)
    proof = concat
      [ [ NN (a :-> b)
        , NN a
        , NN a :-> N b :-> NN a :& N b
        , n4],
        helpers3_1 a b,
        helpers2 n5 (N b),
        doubleMP n4 n6 n9,
        helpers2 (NN (a :-> b)) (N b),
        doubleMP n9 n10 (NN b)
      ]

substitute :: [(Expression, Expression)] -> [Expression] -> [Expression]
substitute substs = map replaceAll
   where
     substMap = M.fromList substs

     replaceAll expr
       | Just new <- M.lookup expr substMap = new  -- might need to use Hashed
     replaceAll (a :-> b)  = on (:->) replaceAll a b
     replaceAll (a :& b)   = on (:&) replaceAll a b
     replaceAll (a :| b)   = on (:|) replaceAll a b
     replaceAll (Not a)    = Not (replaceAll a)
     replaceAll  v@(Var _) = v

helpers0 :: Expression -> [Expression] -- ⊢ a → a
helpers0 ea = substitute [(a, ea)] proof
  where
    a = Var "a"
    proof =
      [ a :-> a :-> a                                             -- 1. sch 1
      , a :-> (a :-> a) :-> a                                     -- 2. sch 1
      , (a :-> a :-> a) :-> (a :-> (a :-> a) :-> a) :-> (a :-> a) -- 3. sch 2
      , (a :-> (a :-> a) :-> a) :-> (a :-> a)                     -- 4. MP 3,1
      , a :-> a                                                   -- 5. MP 4,2
      ]

helpers1 :: Expression -> Expression -> [Expression] -- a → b ⊢ -b → -a
helpers1 ea eb = substitute [(a, ea), (b, eb)] proof
  where
    a = Var "a"
    b = Var "b"
    n1 = N b :-> a :-> N b
    n4 = (a :-> N b) :-> N a
    n6 = N b :-> n4
    proof =
      [ n1                           -- 1. sch 1
      , a :-> b                      -- 2. hyp
      , (a :-> b) :-> n4             -- 3. sch 9
      , n4                           -- 4. MP 3,2
      , n4 :-> n6                    -- 5. sch 1
      , n6                           -- 6. MP 5,4
      , n1 :-> n6 :-> (N b :-> N a)  -- 7. sch 2
      , n6  :-> (N b :-> N a)        -- 8. MP 7,1
      , N b :-> N a                  -- 9. MP 8,6
      ]

helpers1_1 :: Expression -> Expression -> [Expression] -- ⊢ (a → b) → (-b → -a)
helpers1_1 ea eb = annotate hyps stmt proof
                |> introduceHyp hyp
                |> substitute [(a, ea), (b, eb)]
                & either (\s -> [] & debug ("helpers1 problem\n" ++ s)) id
  where
    a = Var "a"
    b = Var "b"
    hyps@(hyp:_) = [a :-> b :: Expression]
    stmt = N b :-> N a
    proof = helpers1 a b

helpers2 :: Expression -> Expression -> [Expression] -- a ⊢ b → a
helpers2 ea eb = substitute [(a, ea), (b, eb)] proof
  where
    a = Var "a"
    b = Var "b"
    proof =
      [ a
      , a :-> b :-> a
      , b :-> a ]

helpers3 :: Expression -> Expression -> [Expression] -- --a & -b ⊢ -(a → b)
helpers3 ea eb = substitute [(a, ea), (b, eb)] proof
  where
    a = Var "a"
    b = Var "b"
    n6 = (a :-> b) :-> N b
    n7 = (a :-> b) :-> (N b :-> N a)
    n10 = (a :-> b) :-> N a
    n11 = (a :-> b) :-> NN a
    proof = concat
      [ [ NN a :& N b
        , NN a :& N b :-> NN a
        , NN a
        , NN a :& N b :-> N b
        , N b]
        , helpers2 (N b) (a :-> b) -- 6. (a → b) → -b
        , helpers1_1 a b           -- 7. (a → b) → (-b → -a)
        , doubleMP n6 n7 n10
        , helpers2 (NN a) (a :-> b) -- 11. (a → b) → --a
        , doubleMP n10 n11 (N (a :-> b))
      ]

helpers3_1 :: Expression -> Expression -> [Expression] -- ⊢ --a & -b → -(a → b)
helpers3_1 ea eb = annotate hyps stmt proof
                |> introduceHyp hyp
                |> substitute [(a, ea), (b, eb)]
                & either (\s -> [] & debug ("helpers3 problem\n" ++ s)) id
  where
    a = Var "a"
    b = Var "b"
    hyps@(hyp:_) = [NN a :& N b :: Expression]
    stmt = N (a :-> b)
    proof = helpers3 a b

doubleMP :: Expression -> Expression -> Expression -> [Expression] -- a, b, a → b → c ⊢ c
doubleMP ea eb ec = substitute [(a, ea), (b, eb), (c, ec)] proof
  where
    a = Var "a"
    b = Var "b"
    c = Var "c"
    proof =
      [ a :-> b :-> c
      , b :-> c
      , c ]

introduceHyp :: Expression -> [Annotated] -> [Expression]
introduceHyp  hyp proof' = concatMap introduceToStmt proof
  where
    proof :: IntArr (Expression, Source)
    proof = extractWithSource proof'
    introduceToStmt :: (Expression, Source) -> [Expression]
    introduceToStmt (expr, src) =
        case src of
            (Ax _) -> helpers2 expr hyp
            (Hyp _)
                | expr /= hyp -> helpers2 expr hyp
                | otherwise -> helpers0 expr
            (MP _ q) ->
                let fterm = proof ! q & fst
                in doubleMP (hyp :-> fterm) (hyp :-> fterm :-> expr) (hyp :-> expr)
