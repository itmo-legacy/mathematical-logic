{
module Parser (parseInfo) where

import Lexer
import Expression (Expression(..))
}

%name parseInfo
%tokentype { Token }
%error { parseError }
%monad { Either String }

%token
  '->'    { ImplT }
  '|'     { DisjT }
  '&'     { ConjT }
  '!'     { NegT }
  '('     { OpParenT }
  ')'     { ClParenT }
  var     { VarT $$ }

%%

expr   :: { Expression }
expr   : disj                      { $1 }
       | disj '->' expr            { $1 :-> $3 }

disj   :: { Expression }
disj   : conj                      { $1 }
       | disj '|' conj             { $1 :| $3 }

conj   :: { Expression }
conj   : neg                       { $1 }
       | conj '&' neg              { $1 :& $3 }

neg    :: { Expression }
neg    : '!' neg                   { Not $2 }
       | var                       { Var $1 }
       | '(' expr ')'              { $2 }

{
parseError = fail "Parsing error"
}
