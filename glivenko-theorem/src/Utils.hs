{-# LANGUAGE BangPatterns #-}

module Utils where

import qualified Data.Set as S
import           System.IO.Unsafe (unsafePerformIO)


infixl 1 |>
(|>) :: (Functor m) => m a -> (a -> b) -> m b
(|>) = flip (<$>)

infixl 1 &!
(&!) :: a -> (a -> b) -> b
(&!) = flip ($!)

nubOrd :: (Ord a) => [a] -> [a]
nubOrd = nubOrdOn id

nubOrdOn :: (Ord b) => (a -> b) -> [a] -> [a]
nubOrdOn f = go S.empty []
  where
    go _ acc [] = reverse acc
    go seen acc (x:xs)
        | prop `S.member` seen = go seen acc xs
        | otherwise = go (S.insert prop seen) acc' xs
      where
        !prop = f x
        !acc' = x:acc

debug :: String -> t -> t
debug msg s = unsafePerformIO (putStrLn msg) `seq` s
