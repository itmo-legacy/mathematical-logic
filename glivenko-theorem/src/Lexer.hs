{-# LANGUAGE DeriveAnyClass    #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}

module Lexer (Token(..), tokenize) where

import           Control.DeepSeq (NFData (..))
import qualified Data.ByteString.Char8 as BS
import           Data.Char (isSpace)
import           Data.Function ((&))
import           GHC.Generics (Generic)

import           Utils

data Token
    = ImplT
    | DisjT
    | ConjT
    | NegT
    | OpParenT
    | ClParenT
    | VarT String
    deriving (Show, Generic, NFData)

tokenize :: BS.ByteString -> Either String [Token]
tokenize s =
    case BS.uncons s of
        Nothing -> return []
        Just (c, cs) ->
            case c of
                '-' -> expect '>' cs |> (ImplT :)
                '|' -> (tokenize $! cs) |> (DisjT :)
                '&' -> (tokenize $! cs) |> (ConjT :)
                '!' -> (tokenize $! cs) |> (NegT :)
                '(' -> (tokenize $! cs) |> (OpParenT :)
                ')' -> (tokenize $! cs) |> (ClParenT :)
                _ | firstInVar c -> lexVar c cs
                  | isSpace c -> tokenize $! cs
                  | otherwise -> Left ("Unexpected symbol: " ++ [c])

expect :: Char -> BS.ByteString -> Either String [Token]
expect c s' =
    case BS.uncons s' of
        Nothing -> Left "Unexpected end of input"
        Just (c', cs')
            | c == c' -> tokenize cs'
            | otherwise -> Left ("Unexpected symbol: " ++ [c])

lexVar :: Char -> BS.ByteString -> Either String [Token]
lexVar first cs =
    BS.span nextInVar cs & \(var, rest) ->
        tokenize rest |> (VarT (first : BS.unpack var) :)

firstInVar :: Char -> Bool
firstInVar c = c `elem` firstL

nextInVar :: Char -> Bool
nextInVar c = c `elem` nextL

firstL :: String
firstL = ['A' .. 'Z']

nextL :: String
nextL = firstL ++ ['0' .. '9'] ++ "'"
